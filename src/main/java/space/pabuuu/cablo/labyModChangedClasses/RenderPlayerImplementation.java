package space.pabuuu.cablo.labyModChangedClasses;

import net.labymod.core.LabyModCore;
import net.labymod.core.RenderPlayerAdapter;
import net.labymod.core.WorldRendererAdapter;
import net.labymod.core_implementation.mc112.layer.LayerArrowCustom;
import net.labymod.core_implementation.mc112.layer.LayerBipedArmorCustom;
import net.labymod.core_implementation.mc112.layer.LayerHeldItemCustom;
import net.labymod.main.LabyMod;
import net.labymod.mojang.RenderPlayerHook.RenderPlayerCustom;
import net.labymod.tags.TagManager;
import net.labymod.userdata.User;
import net.labymod.utils.ModColor;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.AbstractClientPlayer;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.renderer.entity.RenderPlayer;
import net.minecraft.client.renderer.entity.layers.LayerCustomHead;
import net.minecraft.client.renderer.entity.layers.LayerRenderer;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.scoreboard.ScoreObjective;
import org.lwjgl.opengl.GL11;
import space.pabuuu.cablo.player.download.DownloadEars;
import space.pabuuu.cablo.player.render.LayerDeadmau5;
import space.pabuuu.cablo.player.render.LayerCape;
import space.pabuuu.cablo.player.render.LayerElytra;

public class RenderPlayerImplementation implements RenderPlayerAdapter {

  public RenderPlayerImplementation() {
  }

  public String[] getSkinMapNames() {
    return new String[]{"skinMap", "l", "field_178636_l"};
  }

  public LayerRenderer[] getLayerRenderers(RenderPlayer renderPlayer) {
    return new LayerRenderer[]{new LayerBipedArmorCustom(renderPlayer), new LayerHeldItemCustom(renderPlayer), new LayerArrowCustom(renderPlayer), new LayerDeadmau5(renderPlayer), new LayerCape(renderPlayer), new LayerElytra(renderPlayer), new LayerCustomHead(renderPlayer.getMainModel().bipedHead)};
  }

  public void renderName(RenderPlayerCustom renderPlayer, AbstractClientPlayer entity, double x, double y, double z) {
    boolean canRender = Minecraft.isGuiEnabled() && !entity.isInvisibleToPlayer(Minecraft.getMinecraft().player) && entity.getRidingEntity() == null;
    if (renderPlayer.canRenderTheName(entity) || entity == renderPlayer.getRenderManager().renderViewEntity && LabyMod.getSettings().showMyName && canRender) {
      double d0 = entity.getDistanceSq(renderPlayer.getRenderManager().renderViewEntity.getPosition());
      float f = entity.isSneaking() ? 32.0F : 64.0F;
      User user = entity instanceof EntityPlayer ? LabyMod.getInstance().getUserManager().getUser(entity.getUniqueID()) : null;
      float maxNameTagHeight = user != null && LabyMod.getSettings().cosmetics ? user.getMaxNameTagHeight() : 0.0F;
      String displayRank = user != null && user.getRank().isRender() && user.isRankVisible() ? user.getRank().buildTag() : null;
      if (d0 < (double)(f * f)) {
        String username = entity.getDisplayName().getFormattedText();
        float f1 = 0.02666667F;
        GlStateManager.alphaFunc(516, 0.1F);
        String tagName = TagManager.getTaggedMessage(username);
        double subHeight;
        ScoreObjective scoreObjective;
        if (tagName != null) {
          username = tagName;
          subHeight = (double)maxNameTagHeight - 0.01D;
          if (Minecraft.getMinecraft().world != null && Minecraft.getMinecraft().world.getScoreboard() != null) {
            scoreObjective = Minecraft.getMinecraft().world.getScoreboard().getObjectiveInDisplaySlot(2);
            if (scoreObjective != null) {
              subHeight += 0.3D;
            }
          }

          FontRenderer fontrenderer = renderPlayer.getFontRendererFromRenderManager();
          GlStateManager.pushMatrix();
          GlStateManager.translate((double)((float)x), (double)((float)y + entity.height + 0.5F - (entity.isChild() ? entity.height / 2.0F : 0.0F)) + subHeight, (double)((float)z));
          GlStateManager.rotate(-renderPlayer.getRenderManager().playerViewY, 0.0F, 1.0F, 0.0F);
          GlStateManager.rotate(renderPlayer.getRenderManager().playerViewX, 1.0F, 0.0F, 0.0F);
          GlStateManager.scale(-0.01666667F, -0.01666667F, 0.01666667F);
          GlStateManager.translate(0.0F, entity.isSneaking() ? 17.0F : 2.0F, 0.0F);
          GlStateManager.disableLighting();
          GlStateManager.enableBlend();
          fontrenderer.drawString("✎", 5 + (int)((double)fontrenderer.getStringWidth(tagName) * 0.8D), 0, ModColor
              .toRGB(255, 255, 0, 255));
          GlStateManager.disableBlend();
          GlStateManager.enableLighting();
          GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
          GlStateManager.popMatrix();
        }

        if (entity.isSneaking()) {
          FontRenderer fontrenderer = renderPlayer.getFontRendererFromRenderManager();
          GlStateManager.pushMatrix();
          GlStateManager.translate((float)x, (float)y + entity.height + 0.5F - (entity.isChild() ? entity.height / 2.0F : 0.0F) + maxNameTagHeight, (float)z);
          GL11.glNormal3f(0.0F, 1.0F, 0.0F);
          GlStateManager.rotate(-renderPlayer.getRenderManager().playerViewY, 0.0F, 1.0F, 0.0F);
          GlStateManager.rotate(renderPlayer.getRenderManager().playerViewX, 1.0F, 0.0F, 0.0F);
          GlStateManager.scale(-0.02666667F, -0.02666667F, 0.02666667F);
          GlStateManager.translate(0.0F, 9.374999F, 0.0F);
          GlStateManager.disableLighting();
          GlStateManager.depthMask(false);
          GlStateManager.enableBlend();
          GlStateManager.disableTexture2D();
          GlStateManager.tryBlendFuncSeparate(770, 771, 1, 0);
          int i = fontrenderer.getStringWidth(username) / 2;
          Tessellator tessellator = Tessellator.getInstance();
          BufferBuilder worldrenderer = tessellator.getBuffer();
          worldrenderer.begin(7, DefaultVertexFormats.POSITION_COLOR);
          worldrenderer.pos((double)(-i - 1), -1.0D, 0.0D).color(0.0F, 0.0F, 0.0F, 0.25F).endVertex();
          worldrenderer.pos((double)(-i - 1), 8.0D, 0.0D).color(0.0F, 0.0F, 0.0F, 0.25F).endVertex();
          worldrenderer.pos((double)(i + 1), 8.0D, 0.0D).color(0.0F, 0.0F, 0.0F, 0.25F).endVertex();
          worldrenderer.pos((double)(i + 1), -1.0D, 0.0D).color(0.0F, 0.0F, 0.0F, 0.25F).endVertex();
          tessellator.draw();
          GlStateManager.enableTexture2D();
          GlStateManager.depthMask(true);
          fontrenderer.drawString(username, -fontrenderer.getStringWidth(username) / 2, 0, 553648127);
          GlStateManager.enableLighting();
          GlStateManager.disableBlend();
          GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
          GlStateManager.popMatrix();
        } else {
          renderPlayer.renderLabel(entity, x, y - (entity.isChild() ? (double)(entity.height / 2.0F) : 0.0D) + (double)maxNameTagHeight, z, username, 0.02666667F, d0);
          if (displayRank != null && !entity.isElytraFlying()) {
            subHeight = 0.3D + (double)maxNameTagHeight;
            if (Minecraft.getMinecraft().world != null && Minecraft.getMinecraft().world.getScoreboard() != null) {
              scoreObjective = Minecraft.getMinecraft().world.getScoreboard().getObjectiveInDisplaySlot(2);
              if (scoreObjective != null) {
                subHeight += 0.3D;
              }
            }

            GlStateManager.pushMatrix();
            double size = 0.5D;
            GlStateManager.scale(0.5D, 0.5D, 0.5D);
            GlStateManager.translate(0.0D, 2.0D, 0.0D);
            this.renderLivingLabelCustom(renderPlayer, entity, displayRank, x / size, (y - (entity.isChild() ? (double)(entity.height / 2.0F) : 0.0D) + subHeight) / size, z / size, 10);
            GlStateManager.popMatrix();
          }
        }
      }
    }

  }

  private void renderLivingLabelCustom(RenderPlayerCustom renderPlayer, Entity entityIn, String str,
      double x, double y, double z, int maxDistance) {
    double d0 = entityIn.getDistanceSq(renderPlayer.getRenderManager().renderViewEntity.getPosition());
    if (d0 <= (double)(maxDistance * maxDistance)) {
      FontRenderer fontrenderer = renderPlayer.getFontRendererFromRenderManager();
      float f = 1.6F;
      float f1 = 0.016666668F * f;
      GlStateManager.pushMatrix();
      GlStateManager.translate((float)x + 0.0F, (float)y + entityIn.height + 0.5F, (float)z);
      GL11.glNormal3f(0.0F, 1.0F, 0.0F);
      GlStateManager.rotate(-renderPlayer.getRenderManager().playerViewY, 0.0F, 1.0F, 0.0F);
      GlStateManager.rotate(renderPlayer.getRenderManager().playerViewX, 1.0F, 0.0F, 0.0F);
      GlStateManager.scale(-f1, -f1, f1);
      GlStateManager.disableLighting();
      GlStateManager.depthMask(false);
      GlStateManager.disableDepth();
      GlStateManager.enableBlend();
      GlStateManager.tryBlendFuncSeparate(770, 771, 1, 0);
      Tessellator tessellator = Tessellator.getInstance();
      WorldRendererAdapter worldrenderer = LabyModCore.getWorldRenderer();
      int i = 0;
      if (DownloadEars.hasEars(entityIn.getUniqueID().toString().replace("-", ""))) {
        i = -10;
      }

      int j = fontrenderer.getStringWidth(str) / 2;
      GlStateManager.disableTexture2D();
      worldrenderer.begin(7, DefaultVertexFormats.POSITION_COLOR);
      worldrenderer.pos((double)(-j - 1), (double)(-1 + i), 0.0D).color(0.0F, 0.0F, 0.0F, 0.25F).endVertex();
      worldrenderer.pos((double)(-j - 1), (double)(8 + i), 0.0D).color(0.0F, 0.0F, 0.0F, 0.25F).endVertex();
      worldrenderer.pos((double)(j + 1), (double)(8 + i), 0.0D).color(0.0F, 0.0F, 0.0F, 0.25F).endVertex();
      worldrenderer.pos((double)(j + 1), (double)(-1 + i), 0.0D).color(0.0F, 0.0F, 0.0F, 0.25F).endVertex();
      tessellator.draw();
      GlStateManager.enableTexture2D();
      fontrenderer.drawString(str, -fontrenderer.getStringWidth(str) / 2, i, 553648127);
      GlStateManager.enableDepth();
      GlStateManager.depthMask(true);
      fontrenderer.drawString(str, -fontrenderer.getStringWidth(str) / 2, i, -1);
      GlStateManager.enableLighting();
      GlStateManager.disableBlend();
      GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
      GlStateManager.popMatrix();
    }

  }

  public RenderPlayerCustom getRenderPlayer(RenderManager renderManager, boolean slim) {
    return new RenderPlayerCustom(renderManager, slim) {
      public boolean canRenderTheName(AbstractClientPlayer entity) {
        return super.canRenderName(entity);
      }

      public void renderLabel(AbstractClientPlayer entityIn, double x, double y, double z, String string, float height, double distance) {
        super.renderEntityName(entityIn, x, y, z, string, distance);
      }

      public void renderName(AbstractClientPlayer entity, double x, double y, double z) {
        LabyModCore.getRenderPlayerImplementation().renderName(this, entity, x, y, z);
      }
    };
  }
}
