package space.pabuuu.cablo.labyModChangedClasses;

import net.labymod.core.ChunkAdapter;
import net.labymod.core.ForgeAdapter;
import net.labymod.core.GuiExtraChatAdapter;
import net.labymod.core.MappingAdapter;
import net.labymod.core.MathAdapter;
import net.labymod.core.MinecraftAdapter;
import net.labymod.core.RenderAdapter;
import net.labymod.core.RenderPlayerAdapter;
import net.labymod.core.ServerPingerAdapter;
import net.labymod.core.SoundAdapter;
import net.labymod.core.WorldRendererAdapter;

public class CoreImplementation implements net.labymod.core.CoreAdapter {

  private final ForgeAdapter forgeImplementation;
  private final GuiExtraChatAdapter guiChatExtraImplementation;
  private final MathAdapter mathImplementation;
  private final MinecraftAdapter minecraftImplementation;
  private final RenderAdapter renderImplementation;
  private final RenderPlayerAdapter renderPlayerImplementation;
  private final MappingAdapter mappingAdapter;
  private final SoundAdapter soundImplementation;
  private final WorldRendererAdapter worldRendererImplementation;
  private final ServerPingerAdapter serverPingerImplementation;
  private final ChunkAdapter chunkImplementation;

  public CoreImplementation(ForgeAdapter forgeImplementation,
      GuiExtraChatAdapter guiChatExtraImplementation,
      MinecraftAdapter minecraftImplementation, ChunkAdapter chunkImplementation,
      MappingAdapter mappingAdapter,
      RenderPlayerAdapter renderPlayerImplementation,
      RenderAdapter renderImplementation, MathAdapter mathImplementation,
      ServerPingerAdapter serverPingerImplementation,
      SoundAdapter soundImplementation, WorldRendererAdapter worldRendererImplementation) {
    this.forgeImplementation = forgeImplementation;
    this.guiChatExtraImplementation = guiChatExtraImplementation;
    this.mathImplementation = mathImplementation;
    this.minecraftImplementation = minecraftImplementation;
    this.renderImplementation = renderImplementation;
    this.renderPlayerImplementation = renderPlayerImplementation;
    this.mappingAdapter = mappingAdapter;
    this.soundImplementation = soundImplementation;
    this.worldRendererImplementation = worldRendererImplementation;
    this.serverPingerImplementation = serverPingerImplementation;
    this.chunkImplementation = chunkImplementation;
  }

  @Override
  public ForgeAdapter getForgeImplementation() {
    return this.forgeImplementation;
  }

  @Override
  public GuiExtraChatAdapter getGuiChatExtraImplementation() {
    return this.guiChatExtraImplementation;
  }

  @Override
  public MathAdapter getMathImplementation() {
    return this.mathImplementation;
  }

  @Override
  public MinecraftAdapter getMinecraftImplementation() {
    return this.minecraftImplementation;
  }

  @Override
  public RenderAdapter getRenderImplementation() {
    return this.renderImplementation;
  }

  @Override
  public RenderPlayerAdapter getRenderPlayerImplementation() {
    return this.renderPlayerImplementation;
  }

  @Override
  public SoundAdapter getSoundImplementation() {
    return this.soundImplementation;
  }

  @Override
  public WorldRendererAdapter getWorldRendererImplementation() {
    return this.worldRendererImplementation;
  }

  @Override
  public MappingAdapter getMappingAdapter() {
    return this.mappingAdapter;
  }

  @Override
  public ServerPingerAdapter getServerPingerImplementation() {
    return this.serverPingerImplementation;
  }

  @Override
  public ChunkAdapter getChunkAdapter() {
    return this.chunkImplementation;
  }
}
