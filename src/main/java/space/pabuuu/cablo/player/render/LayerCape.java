package space.pabuuu.cablo.player.render;

import java.util.ArrayList;
import java.util.Iterator;
import net.labymod.core.LabyModCore;
import net.labymod.cosmetic.custom.cape.Star;
import net.labymod.ingamegui.Module;
import net.labymod.main.LabyMod;
import net.labymod.main.ModTextures;
import net.minecraft.client.entity.AbstractClientPlayer;
import net.minecraft.client.network.NetworkPlayerInfo;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.entity.RenderPlayer;
import net.minecraft.client.renderer.entity.layers.LayerRenderer;
import net.minecraft.entity.player.EnumPlayerModelParts;
import net.minecraft.init.Items;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import space.pabuuu.cablo.CabloAddon;
import space.pabuuu.cablo.player.download.DownloadCape;

public class LayerCape implements LayerRenderer<AbstractClientPlayer> {
  private final RenderPlayer playerRenderer;
  private ArrayList<Star> stars;
  private long lastSpawn = 0L;

  public LayerCape(RenderPlayer playerRendererIn) {
    this.playerRenderer = playerRendererIn;
  }

  @Override
  public void doRenderLayer(AbstractClientPlayer entitylivingbaseIn, float var1, float var2, float partialTicks, float var3, float var4, float var5, float scale) {
    if (CabloAddon.enabledCape) {
      renderLayerMcCapes(entitylivingbaseIn, var1, var2, partialTicks, var3, var4, var5, scale);
    } else {
      renderLayerDefault(entitylivingbaseIn, var1, var2, partialTicks, var3, var4, var5, scale);
    }
  }


  private void renderLayerMcCapes(AbstractClientPlayer entitylivingbaseIn, float var1, float var2, float partialTicks, float var3, float var4, float var5, float scale) {
    if (entitylivingbaseIn.hasPlayerInfo() && !entitylivingbaseIn.isInvisible() && entitylivingbaseIn.isWearing(EnumPlayerModelParts.CAPE)) {
      ItemStack itemstack = entitylivingbaseIn.getItemStackFromSlot(EntityEquipmentSlot.CHEST);
      if (itemstack.getItem() == Items.ELYTRA) {
        return;
      }

      GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
      ResourceLocation capeLocation = DownloadCape.getCapeResourceLocation(entitylivingbaseIn);
      if (capeLocation == null) {
        return;
      }

      this.playerRenderer.bindTexture(capeLocation);
      GlStateManager.pushMatrix();
      GlStateManager.translate(0.0F, 0.0F, 0.125F);
      double d0 = entitylivingbaseIn.prevChasingPosX + (entitylivingbaseIn.chasingPosX - entitylivingbaseIn.prevChasingPosX) * (double)partialTicks - (entitylivingbaseIn.prevPosX + (entitylivingbaseIn.posX - entitylivingbaseIn.prevPosX) * (double)partialTicks);
      double d1 = entitylivingbaseIn.prevChasingPosY + (entitylivingbaseIn.chasingPosY - entitylivingbaseIn.prevChasingPosY) * (double)partialTicks - (entitylivingbaseIn.prevPosY + (entitylivingbaseIn.posY - entitylivingbaseIn.prevPosY) * (double)partialTicks);
      double d2 = entitylivingbaseIn.prevChasingPosZ + (entitylivingbaseIn.chasingPosZ - entitylivingbaseIn.prevChasingPosZ) * (double)partialTicks - (entitylivingbaseIn.prevPosZ + (entitylivingbaseIn.posZ - entitylivingbaseIn.prevPosZ) * (double)partialTicks);
      float f = entitylivingbaseIn.prevRenderYawOffset + (entitylivingbaseIn.renderYawOffset - entitylivingbaseIn.prevRenderYawOffset) * partialTicks;
      double d3 = (double)LabyModCore.getMath().sin(f * 3.1415927F / 180.0F);
      double d4 = (double)(-LabyModCore.getMath().cos(f * 3.1415927F / 180.0F));
      float f1 = (float)d1 * 10.0F;
      f1 = LabyModCore.getMath().clamp_float(f1, -6.0F, 32.0F);
      float f2 = (float)(d0 * d3 + d2 * d4) * 100.0F;
      float f3 = (float)(d0 * d4 - d2 * d3) * 100.0F;
      if (f2 < 0.0F) {
        f2 = 0.0F;
      }

      if (f2 >= 180.0F) {
        f2 = 180.0F + (f2 - 180.0F) * 0.2F;
      }

      float f4 = entitylivingbaseIn.prevCameraYaw + (entitylivingbaseIn.cameraYaw - entitylivingbaseIn.prevCameraYaw) * partialTicks;
      f1 += LabyModCore.getMath().sin((entitylivingbaseIn.prevDistanceWalkedModified + (entitylivingbaseIn.distanceWalkedModified - entitylivingbaseIn.prevDistanceWalkedModified) * partialTicks) * 6.0F) * 32.0F * f4;
      if (entitylivingbaseIn.isSneaking()) {
        GlStateManager.translate(0.0F, 0.05F, -0.05F);
        f1 += 25.0F;
      }

      GlStateManager.rotate(6.0F + f2 / 2.0F + f1, 1.0F, 0.0F, 0.0F);
      GlStateManager.rotate(f3 / 2.0F, 0.0F, 0.0F, 1.0F);
      GlStateManager.rotate(-f3 / 2.0F, 0.0F, 1.0F, 0.0F);
      GlStateManager.rotate(180.0F, 0.0F, 1.0F, 0.0F);
      this.playerRenderer.getMainModel().renderCape(0.0625F);
      if (LabyMod.getSettings().mineconParticles && CabloAddon.mineconParticles) {
          renderStars();
      }

      GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
      GlStateManager.popMatrix();
    }
  }
  private void renderLayerDefault(AbstractClientPlayer entitylivingbaseIn, float var1, float var2, float partialTicks, float var3, float var4, float var5, float scale) {
    if (entitylivingbaseIn.hasPlayerInfo() && !entitylivingbaseIn.isInvisible() && entitylivingbaseIn.isWearing(EnumPlayerModelParts.CAPE)) {
      ItemStack itemstack = entitylivingbaseIn.getItemStackFromSlot(EntityEquipmentSlot.CHEST);
      if (itemstack.getItem() == Items.ELYTRA) {
        return;
      }

      GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
      ResourceLocation capeLocation = LabyMod.getInstance().getUserManager().getCosmeticImageManager().getCapeImageHandler().getResourceLocation(entitylivingbaseIn);
      if (capeLocation == null) {
        return;
      }

      this.playerRenderer.bindTexture(capeLocation);
      GlStateManager.pushMatrix();
      GlStateManager.translate(0.0F, 0.0F, 0.125F);
      double d0 = entitylivingbaseIn.prevChasingPosX + (entitylivingbaseIn.chasingPosX - entitylivingbaseIn.prevChasingPosX) * (double)partialTicks - (entitylivingbaseIn.prevPosX + (entitylivingbaseIn.posX - entitylivingbaseIn.prevPosX) * (double)partialTicks);
      double d1 = entitylivingbaseIn.prevChasingPosY + (entitylivingbaseIn.chasingPosY - entitylivingbaseIn.prevChasingPosY) * (double)partialTicks - (entitylivingbaseIn.prevPosY + (entitylivingbaseIn.posY - entitylivingbaseIn.prevPosY) * (double)partialTicks);
      double d2 = entitylivingbaseIn.prevChasingPosZ + (entitylivingbaseIn.chasingPosZ - entitylivingbaseIn.prevChasingPosZ) * (double)partialTicks - (entitylivingbaseIn.prevPosZ + (entitylivingbaseIn.posZ - entitylivingbaseIn.prevPosZ) * (double)partialTicks);
      float f = entitylivingbaseIn.prevRenderYawOffset + (entitylivingbaseIn.renderYawOffset - entitylivingbaseIn.prevRenderYawOffset) * partialTicks;
      double d3 = (double)LabyModCore.getMath().sin(f * 3.1415927F / 180.0F);
      double d4 = (double)(-LabyModCore.getMath().cos(f * 3.1415927F / 180.0F));
      float f1 = (float)d1 * 10.0F;
      f1 = LabyModCore.getMath().clamp_float(f1, -6.0F, 32.0F);
      float f2 = (float)(d0 * d3 + d2 * d4) * 100.0F;
      float f3 = (float)(d0 * d4 - d2 * d3) * 100.0F;
      if (f2 < 0.0F) {
        f2 = 0.0F;
      }

      if (f2 >= 180.0F) {
        f2 = 180.0F + (f2 - 180.0F) * 0.2F;
      }

      float f4 = entitylivingbaseIn.prevCameraYaw + (entitylivingbaseIn.cameraYaw - entitylivingbaseIn.prevCameraYaw) * partialTicks;
      f1 += LabyModCore.getMath().sin((entitylivingbaseIn.prevDistanceWalkedModified + (entitylivingbaseIn.distanceWalkedModified - entitylivingbaseIn.prevDistanceWalkedModified) * partialTicks) * 6.0F) * 32.0F * f4;
      if (entitylivingbaseIn.isSneaking()) {
        GlStateManager.translate(0.0F, 0.05F, -0.05F);
        f1 += 25.0F;
      }

      GlStateManager.rotate(6.0F + f2 / 2.0F + f1, 1.0F, 0.0F, 0.0F);
      GlStateManager.rotate(f3 / 2.0F, 0.0F, 0.0F, 1.0F);
      GlStateManager.rotate(-f3 / 2.0F, 0.0F, 1.0F, 0.0F);
      GlStateManager.rotate(180.0F, 0.0F, 1.0F, 0.0F);
      this.playerRenderer.getMainModel().renderCape(0.0625F);
      if (LabyMod.getSettings().mineconParticles) {
        NetworkPlayerInfo playerInfo = LabyModCore.getMinecraft().getConnection().getPlayerInfo(entitylivingbaseIn.getUniqueID());
        ResourceLocation resourceLocation = playerInfo == null ? null : playerInfo.getLocationCape();
        if (resourceLocation != null && capeLocation.equals(resourceLocation)) {
          renderStars();
        }
      }

      GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
      GlStateManager.popMatrix();
    }

  }

  private void renderStars() {
    if (this.stars == null) {
      this.stars = new ArrayList<>();
    }

    Module.mc.getTextureManager().bindTexture(ModTextures.COSMETIC_CAPE_STAR);
    if (this.lastSpawn < System.currentTimeMillis()) {
      this.stars.add(new Star(LabyMod.getRandom()));
      this.lastSpawn = System.currentTimeMillis() + (long)LabyMod.getRandom().nextInt(1000);
    }

    Star star;
    for(Iterator it = this.stars.iterator(); it.hasNext(); star.draw()) {
      star = (Star)it.next();
      if (star.isKilled()) {
        it.remove();
      }
    }
  }
  public boolean shouldCombineTextures() {
    return false;
  }
}
