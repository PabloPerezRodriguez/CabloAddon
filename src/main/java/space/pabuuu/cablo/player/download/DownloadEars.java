package space.pabuuu.cablo.player.download;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.IImageBuffer;
import net.minecraft.client.renderer.ImageBufferDownload;
import net.minecraft.client.renderer.ThreadDownloadImageData;
import net.minecraft.client.renderer.texture.ITextureObject;
import net.minecraft.client.renderer.texture.TextureManager;
import net.minecraft.client.resources.IResource;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.ResourceLocation;
import space.pabuuu.cablo.CabloAddon;

/*    */


public class DownloadEars
{
  private static final HashMap<String, Boolean> playersEars = new HashMap<>();
  private static final List<String> downloadedPlayers = new ArrayList<>();

  private static void addDownloadedPlayer(String s) {
    if (!downloadedPlayers.contains(s)) downloadedPlayers.add(s);
  }

  public static Boolean hasEars(String uuid) {
    Boolean hashMapResult = playersEars.get(uuid);
    if (hashMapResult == null) return false;
    return hashMapResult;
  }

  private static void download(String uuid) {
    System.out.println("UUID:" + uuid);
    if ((uuid != null) && (!uuid.isEmpty())) {
      addDownloadedPlayer(uuid);
      String url = CabloAddon.API_URL + "/ears/uuid/" + uuid;
      ResourceLocation rl = new ResourceLocation("ears/" + uuid);
      if (!resourceExists(rl)) {
        TextureManager textureManager = Minecraft.getMinecraft().getTextureManager();
        ITextureObject tex = textureManager.getTexture(rl);

        IImageBuffer iib = new IImageBuffer() {
          ImageBufferDownload ibd = new ImageBufferDownload();

          public BufferedImage parseUserSkin(BufferedImage var1) {
            return DownloadEars.parseEars(var1, uuid);
          }

          public void skinAvailable() {}
        };
        ThreadDownloadImageData textureCape = new ThreadDownloadImageData(null, url, null, iib);
        textureManager.loadTexture(rl, textureCape);
      }
    }
  }

  private static BufferedImage parseEars(BufferedImage img, String uuid) {
    int imageWidth = 64;
    int imageHeight = 64;

    BufferedImage imgNew = new BufferedImage(imageWidth, imageHeight, BufferedImage.TYPE_INT_ARGB);
    Graphics g = imgNew.getGraphics();
    g.drawImage(img, 24, 0, null);
    g.dispose();

    if (CabloAddon.isEmpty(img)) {
      playersEars.put(uuid, false);
      return imgNew;
    } else {
      playersEars.put(uuid, true);
      return imgNew;
    }
  }

  public static ResourceLocation getEarsResourceLocation(EntityLivingBase entitylivingbaseIn) {
    String playerUUID = entitylivingbaseIn.getUniqueID().toString().replace("-", "");
    ResourceLocation resourceLocation = new ResourceLocation("ears/" + playerUUID);
    if (!downloadedPlayers.contains(playerUUID)) {
      download(playerUUID);
    }
    return hasEars(playerUUID) ? resourceLocation : null;
  }

  private static boolean resourceExists(ResourceLocation resourceLocation) {
    try {
      IResource resource = Minecraft.getMinecraft().getResourceManager().getResource(resourceLocation);
      if (resource != null)
        return true;
    } catch (IOException localIOException) {}
    return false;
  }
}
