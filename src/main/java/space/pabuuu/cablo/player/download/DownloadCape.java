package space.pabuuu.cablo.player.download;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.IImageBuffer;
import net.minecraft.client.renderer.ImageBufferDownload;
import net.minecraft.client.renderer.ThreadDownloadImageData;
import net.minecraft.client.renderer.texture.ITextureObject;
import net.minecraft.client.renderer.texture.TextureManager;
import net.minecraft.client.resources.IResource;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.ResourceLocation;
import space.pabuuu.cablo.CabloAddon;

public class DownloadCape {
  private static final HashMap<String, Boolean> playersCape = new HashMap<>();
  private static final HashMap<String, Boolean> playersElytra = new HashMap<>();
  private static final List<String> downloadedPlayers = new ArrayList<>();

  private static void addDownloadedPlayer(String s) {
    if (!downloadedPlayers.contains(s)) downloadedPlayers.add(s);
  }
  private static void download(String uuid) {
    System.out.println("UUID:" + uuid);
    if ((uuid != null) && (!uuid.isEmpty())) {
      addDownloadedPlayer(uuid);
      String url = CabloAddon.API_URL + "/capes/uuid/" + uuid;
      ResourceLocation rl = new ResourceLocation("capes/" + uuid);
      if (!resourceExists(rl)) {
        TextureManager textureManager = Minecraft.getMinecraft().getTextureManager();
        ITextureObject tex = textureManager.getTexture(rl);

        IImageBuffer iib = new IImageBuffer() {
          ImageBufferDownload ibd = new ImageBufferDownload();

          public BufferedImage parseUserSkin(BufferedImage var1) {
            return DownloadCape.parseCape(var1, uuid);
          }


          public void skinAvailable() {}
        };
        ThreadDownloadImageData textureCape = new ThreadDownloadImageData(null, url, null, iib);
        textureManager.loadTexture(rl, textureCape);
      }
    }
  }

  private static BufferedImage parseCape(BufferedImage img, String uuid) {
    int imageWidth = 64;
    int imageHeight = 32;

    int srcWidth = img.getWidth();
    int srcHeight = img.getHeight();
    while ((imageWidth < srcWidth) || (imageHeight < srcHeight)) {
      imageWidth *= 2;
      imageHeight *= 2;
    }

    BufferedImage imgNew = new BufferedImage(imageWidth, imageHeight, BufferedImage.TYPE_INT_ARGB);
    Graphics g = imgNew.getGraphics();
    g.drawImage(img, 0, 0, null);
    g.dispose();

    if (CabloAddon.isEmpty(img)) {
      playersCape.put(uuid, false);
      playersElytra.put(uuid, false);
      return imgNew;
    } else {
      playersCape.put(uuid, true);
      // Invisible elytra are not a good thing.
      playersElytra.put(uuid, !isElytraTexutreEmpty(imgNew));

//    System.out.println(imageWidth);
//    System.out.println(Arrays.toString(pixels));
//    byte[] pixels = ((DataBufferByte) img.getRaster().getDataBuffer()).getData();
//    System.out.println(pixels);
//    isElytraTexutreEmpty(img);

      downloadedPlayers.add(uuid);
      return imgNew;
    }
  }

  private static Boolean hasElytra(String uuid) {
    Boolean hashMapResult = playersElytra.get(uuid);
    if (hashMapResult == null) return false;
    return hashMapResult;
  }

  private static Boolean hasCape(String uuid) {
    Boolean hashMapResult = playersCape.get(uuid);
    if (hashMapResult == null) return false;
    return hashMapResult;
  }

  private static boolean isElytraTexutreEmpty(BufferedImage img) {
    /*
       x=22, y=0 ______________ x=50, y=0
          |                        |
          |                        |
          |         29x26          |                 Elytra texture
          |                        |
          |                        |
       x=22, y=-25 ____________ x=50, y-25
    */

    int width = 29;
    int height = 26;
    BufferedImage newImage = img.getSubimage(22, 0, width, height);
    int[][] pixels2D = CabloAddon.bufferedImageTo2DArray(newImage);


//    System.out.println(Arrays.toString(pixels2D));

    // Presupongo que está vacía
    boolean empty = true;
    for (int[] x : pixels2D) {
      for (int y : x) {
//        System.out.println(y);
        if (y != 0 && y != 1) {
          // Este pixel es de un color que no es ni blanco ni transparente, por tanto hay algo en la textura de elytra
          // Y no está vacía
          empty = false;
          break;
        }
      }
      if (!empty) break;
    }

    return empty;
  }

  public static ResourceLocation getCapeResourceLocation(EntityLivingBase entitylivingbaseIn) {
    String playerUUID = entitylivingbaseIn.getUniqueID().toString().replace("-", "");
    ResourceLocation resourceLocation = new ResourceLocation("capes/" + playerUUID);
    if (!downloadedPlayers.contains(playerUUID)) {
      download(playerUUID);
    }
    return hasCape(playerUUID) ? resourceLocation : null;
  }

  public static ResourceLocation getElytraResourceLocation(EntityLivingBase entitylivingbaseIn) {
    String playerUUID = entitylivingbaseIn.getUniqueID().toString().replace("-", "");
    ResourceLocation resourceLocation = new ResourceLocation("capes/" + playerUUID);
    if (!downloadedPlayers.contains(playerUUID)) {
      download(playerUUID);
    }
    return hasElytra(playerUUID) ? resourceLocation : null;
  }

  private static boolean resourceExists(ResourceLocation resourceLocation) {
    try {
      IResource resource = Minecraft.getMinecraft().getResourceManager().getResource(resourceLocation);
      if (resource != null)
        return true;
    } catch (IOException localIOException) {}
    return false;
  }
}
