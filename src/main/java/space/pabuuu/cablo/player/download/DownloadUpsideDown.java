package space.pabuuu.cablo.player.download;

import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import org.apache.commons.io.IOUtils;
import space.pabuuu.cablo.CabloAddon;

class DownloadUpsideDown {
  private static final HashMap<String, Boolean> playersUpsideDown = new HashMap<>();

  public static Boolean isUpsideDown(String uuid) {
    Boolean hashMapResult = playersUpsideDown.get(uuid);
    if (hashMapResult == null) return false;
    return hashMapResult;
  }

  public static void download(String uuid) {
    System.out.println("UUID:"   + uuid);
    if ((uuid != null) && (!uuid.isEmpty())) {
      String url = CabloAddon.API_URL + "/upsidedown/uuid/" + uuid;
      Thread thread = new Thread(() -> {
        try {
          URL urlObject = new URL(url);
          URLConnection con = urlObject.openConnection();
          InputStream in = con.getInputStream();
          String encoding = con.getContentEncoding();
          encoding = encoding == null ? "UTF-8" : encoding;
          String body = IOUtils.toString(in, encoding);
          parseUpsideDown(body, uuid);
        } catch (Exception e) {
          e.printStackTrace();
        }
      });
      thread.start();
    }
  }

  private static void parseUpsideDown(String body, String uuid) {
    if (body.equals("true")) {
      playersUpsideDown.put(uuid, true);
    } else if (body.equals("false")) {
      playersUpsideDown.put(uuid, false);
    }
  }
}
