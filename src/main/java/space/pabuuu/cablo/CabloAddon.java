package space.pabuuu.cablo;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.util.List;
import net.labymod.api.LabyModAddon;
import net.labymod.core.ChunkAdapter;
import net.labymod.core.CoreAdapter;
import net.labymod.core.ForgeAdapter;
import net.labymod.core.GuiExtraChatAdapter;
import net.labymod.core.LabyModCore;
import net.labymod.core.MappingAdapter;
import net.labymod.core.MathAdapter;
import net.labymod.core.MinecraftAdapter;
import net.labymod.core.RenderAdapter;
import net.labymod.core.RenderPlayerAdapter;
import net.labymod.core.ServerPingerAdapter;
import net.labymod.core.SoundAdapter;
import net.labymod.core.WorldRendererAdapter;
import net.labymod.settings.elements.BooleanElement;
import net.labymod.settings.elements.ControlElement.IconData;
import net.labymod.settings.elements.SettingsElement;
import net.labymod.utils.Material;
import space.pabuuu.cablo.labyModChangedClasses.CoreImplementation;
import space.pabuuu.cablo.labyModChangedClasses.RenderPlayerImplementation;

@SuppressWarnings("unused")
public class CabloAddon extends LabyModAddon {
  public static boolean enabledCape;
  public static boolean mineconParticles;

  public static final String API_URL = "http://api.uhc.pabuuu.space";
  public static int[][] bufferedImageTo2DArray(BufferedImage imageIn) {
    BufferedImage image = new BufferedImage(imageIn.getWidth(), imageIn.getWidth(), BufferedImage.TYPE_INT_ARGB);
    Graphics g = image.getGraphics();
    g.drawImage(imageIn, 0, 0, null);
    g.dispose();
    int[][] pixels2D = new int[image.getWidth()][image.getHeight()]; // x, y
    int[] pixels = ((DataBufferInt) image.getRaster().getDataBuffer()).getData();
    for (int row = 0; row < image.getHeight(); row++) { // height = rows
      for (int column = 0; column < image.getWidth(); column++) { // width = columns
        pixels2D[column][row] = pixels[row * image.getWidth() + column];
      }
    }

    return pixels2D;
  }
  public static boolean isEmpty(BufferedImage img) {
    return (img.getWidth() < 64 || img.getHeight() < 32);
  }

  /**
   * Called when the addon gets enabled
   */
  @Override
  public void onEnable() {
    System.out.println("ENABLED");
    changeRenderPlayerImplementation();
  }

  private void changeRenderPlayerImplementation() {
    CoreAdapter coreAdapter = LabyModCore.getCoreAdapter();
    MinecraftAdapter minecraftImplementation = coreAdapter.getMinecraftImplementation();
    ChunkAdapter chunkAdapter = coreAdapter.getChunkAdapter();
    MappingAdapter mappingAdapter = coreAdapter.getMappingAdapter();
    RenderPlayerAdapter renderPlayerImplementation = new RenderPlayerImplementation();
    RenderAdapter renderImplementation = coreAdapter.getRenderImplementation();
    ForgeAdapter forgeImplementation = coreAdapter.getForgeImplementation();
    GuiExtraChatAdapter guiChatExtraImplementation = coreAdapter.getGuiChatExtraImplementation();
    MathAdapter mathImplementation = coreAdapter.getMathImplementation();
    ServerPingerAdapter serverPingerImplementation = coreAdapter.getServerPingerImplementation();
    SoundAdapter soundImplementation = coreAdapter.getSoundImplementation();
    WorldRendererAdapter worldRendererImplementation = coreAdapter.getWorldRendererImplementation();

    CoreAdapter newCoreAdapter = new CoreImplementation(forgeImplementation, guiChatExtraImplementation, minecraftImplementation, chunkAdapter, mappingAdapter, renderPlayerImplementation, renderImplementation, mathImplementation, serverPingerImplementation, soundImplementation, worldRendererImplementation);
    LabyModCore.setCoreAdapter(newCoreAdapter);
  }

  /**
   * Called when the addon gets disabled
   */
  @Override
  public void onDisable() {

  }

  /**
   * Called when this addon's config was loaded and is ready to use
   */
  @Override
  public void loadConfig() {
    enabledCape = !getConfig().has("enabledCape") || getConfig().get("enabledCape").getAsBoolean(); // <- default value 'true'
    mineconParticles = !getConfig().has("mineconParticles") || getConfig().get("mineconParticles").getAsBoolean(); // <- default value 'true'
  }

  /**
   * Called when the addon's ingame settings should be filled
   *
   * @param subSettings a list containing the addon's settings' elements
   */
  @Override
  protected void fillSettings( List<SettingsElement> subSettings ) {
    // Toggle element
    subSettings.add(new BooleanElement("Cape enabled", this, new IconData(Material.LEVER), "enabledCape", enabledCape));
    subSettings.add(new BooleanElement("Minecon particles", this, new IconData(Material.LEVER), "mineconParticles", mineconParticles));
  }

}
